var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var gesloGledeNaKanal = {};


exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj', "");
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZasebca(socket);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    
    socket.on('osvezi', function() {
      osvezi(this);
    });

    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}


function osvezi(socket){
  
  var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[socket.id]);
   var stevec = 1;
   var gostje = [];
   gostje[0] = (vzdevkiGledeNaSocket[socket.id]);
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        gostje[stevec] = vzdevkiGledeNaSocket[uporabnikSocketId];
        stevec++;
      }
    } 
  
        socket.emit('prikazGostov', {
          stevec: stevec,
          gostje: gostje
          
        });
  
}

function pridruzitevKanalu(socket, kanal, geslo) {
  socket.join(kanal);
   var uporabnikiNaKanalu = io.sockets.clients(kanal);
   gesloGledeNaKanal[kanal] = geslo;
   var stevec = 1;
   var gostje = [];
   gostje[0] = (vzdevkiGledeNaSocket[socket.id]);
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        gostje[stevec] = vzdevkiGledeNaSocket[uporabnikSocketId];
        stevec++;
      }
    }  
     
    trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {
    kanal: kanal,
    gosti: gostje,
    stevec: stevec,
    vzdevek: vzdevkiGledeNaSocket[socket.id]
    
  });

  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
   


  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {
      besedilo: uporabnikiNaKanaluPovzetek
      
      
    });
  }
}


function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        var kanal = trenutniKanal[socket.id];
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: kanal
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}


function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
   
      
    var obstaja = 0;
    var vsiKanali =  trenutniKanal;
    
  
    for (var i in trenutniKanal) {
       if(kanal.novKanal == vsiKanali[i]){ 
         obstaja = 1;
       }
    }
    
    if(obstaja == 0) {
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
    }
   
    else{
      if(toText(gesloGledeNaKanal[kanal.novKanal]) == "" && toText(kanal.geslo) != "") {
        socket.emit('niGesla', {kanal: kanal.novKanal});
      }
      else if(toText(kanal.geslo) != toText(gesloGledeNaKanal[kanal.novKanal])) {
         socket.emit('napacnoGeslo', {kanal: kanal.novKanal});
      }else{ 
        socket.leave(trenutniKanal[socket.id]);
        pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
      }
    }
  });
  
  
}

function toText(besedilo) {
  return besedilo.toString();
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
    
  });
}


function obdelajZasebca(socket) {
  socket.on('zasebnoSporocilo',  function (dobljeno) {
      var clients = [];
      var stevec = 0;
      
      var uporabnikiNaKanalu = io.sockets.clients();
      for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        clients[stevec] = vzdevkiGledeNaSocket[uporabnikSocketId];
        stevec++;
    }  
    
      var obstaja = -1;
      for (var i = 0; i < stevec; i++) {
         if(dobljeno.prejemnik == clients[i] && uporabnikiNaKanalu[i].id != socket.id){
         
          obstaja = 1;
          uporabnikiNaKanalu[i].emit('zasebnoSporocilo', {
          sporocilo: dobljeno.sporocilo,
          prejemnik: dobljeno.prejemnik,
          posiljatelj: vzdevkiGledeNaSocket[socket.id]
        });
        socket.emit('poslano', {
           prejemnik: dobljeno.prejemnik,
           sporocilo: dobljeno.sporocilo
        });
      }
      
      
      
    }  
    if(obstaja == -1) {
        socket.emit('neObstaja', {
          sporocilo: dobljeno.sporocilo,
          prejemnik: dobljeno.prejemnik
        });
      }
       
  });
}


