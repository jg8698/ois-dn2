var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


Klepet.prototype.spremeniKanal = function(kanal,gesloo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: gesloo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  $('sporocila').empty();
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal =  besede[0];
        besede.shift();
        besede.join(' ');
        var geslo1 = besede;
        geslo1 = besede.toString();
    
        if(geslo1 == '' || geslo1 == '""') {
           if(kanal.charAt(0) != '"' || kanal.charAt(kanal.length-1) != '"') {
              sporocilo = 'Neznan ukaz.';
              break;
           }
           kanal = kanal.substring(1,kanal.length-1); 
           geslo1 = '';
           this.spremeniKanal(kanal,geslo1);
        }
       
        else{
          if(kanal.charAt(0) != '"' || kanal.charAt(kanal.length-1) != '"' || geslo1.charAt(0) != '"' || geslo1.charAt(geslo1.length-1) != '"' ) {
             sporocilo = 'Neznan ukaz.';
              break;
          }  
          kanal = kanal.substring(1,kanal.length-1); 
         geslo1 = geslo1.substring(1,geslo1.length-1); 
          this.spremeniKanal(kanal,geslo1);
        }
      break;
      
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
     case 'zasebno':
       besede.shift();
      var prejemnik = besede[0];
      
      besede.shift();
      var sporocilo1 = besede.join(" ");
      
      if(prejemnik.charAt(0) != '"' || prejemnik.charAt(prejemnik.length-1) != '"' || sporocilo1.charAt(0) != '"' || sporocilo1.charAt(sporocilo1.length-1) != '"') {
        sporocilo = 'Neznan ukaz.';
        break;
      }
      prejemnik = prejemnik.substring(1,prejemnik.length-1); 
      sporocilo1 = sporocilo1.substring(1,sporocilo1.length-1);
      
      this.socket.emit('zasebnoSporocilo', {
      prejemnik: prejemnik,
      sporocilo: sporocilo1
      });
      break;
      
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};