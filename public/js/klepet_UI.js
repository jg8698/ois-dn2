
$('#badWords').load('swearWords.txt');

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

 function zamenjaj(sporocilo) {
  for(var i = 0; i < sporocilo.length; i++) {
      sporocilo = sporocilo.replace(';)','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>');
      sporocilo = sporocilo.replace(':)','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>');
      sporocilo = sporocilo.replace('(y)','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>');
      sporocilo = sporocilo.replace(':*','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>');
      sporocilo = sporocilo.replace(':(','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>');
    }
  return sporocilo;
}



function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


function vrniKanalcek(besedilo) {
  var indeks = besedilo.indexOf("@");
  var nova = besedilo.substring(indeks+1, besedilo.length);
  
}


String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
}

function nePreklinjaj(sporocilo, slovar) {
 var slovarRazbit = slovar.split('\n');
 var maloSproocilo = sporocilo.toLowerCase();
 
 for(var i = 0; i < slovarRazbit.length; i++) {
    if(maloSproocilo.indexOf(slovarRazbit[i].toLowerCase()) >= 0) {
      var indeks = maloSproocilo.indexOf(slovarRazbit[i].toLowerCase());
     while(indeks != -1){
     
     if(indeks == 0 || maloSproocilo.charAt(indeks -1) == ' '){
       if(indeks + slovarRazbit[i].length == maloSproocilo.length){
              for(var j = 0; j < slovarRazbit[i].length; j++) {
                sporocilo = sporocilo.replaceAt(indeks + j,'*');
              }
      }
        if(indeks + slovarRazbit[i].length < maloSproocilo.length){
               if(maloSproocilo.charAt(indeks + slovarRazbit[i].length) == ' ' || maloSproocilo.charAt(indeks + slovarRazbit[i].length) == '?' || maloSproocilo.charAt(indeks + slovarRazbit[i].length) == '.' || maloSproocilo.charAt(indeks + slovarRazbit[i].length) == ',' || maloSproocilo.charAt(indeks + slovarRazbit[i].length) == '!' || maloSproocilo.charAt(indeks + slovarRazbit[i].length) == '-'){
                  for(var k = 0; k < slovarRazbit[i].length; k++) {
                    sporocilo = sporocilo.replaceAt(indeks + k,'*');
                  }
               }
        }
     }
     indeks = maloSproocilo.indexOf(slovarRazbit[i].toLowerCase(),(indeks + 1));
    }
    } 
 }
  
  return sporocilo; 
}



function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
 var badWords = $('#badWords').text();
    klepetApp.posljiSporocilo(vrniKanalcek($('#kanal').text()), sporocilo);
     var stevilo = sporocilo.length;
      sporocilo = zamenjaj(sporocilo);
     if(stevilo == sporocilo.length){
       sporocilo = nePreklinjaj(sporocilo,badWords);
       var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo);
       $('#sporocila').append(novElement);}//napiše meni na ekran moje psoro
     else{ 
       sporocilo = nePreklinjaj(sporocilo,badWords);
        var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo);
       $('#sporocila').append(novElement);}
       $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek + '@' + rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });



  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek + '@' + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
     $('#seznam-gostov').text("");
    for(var i = 0; i < rezultat.stevec; i++) {
       var novElement = $('<div style="font-weight: bold"></div>').text(rezultat.gosti[i]);
     
      $('#seznam-gostov').append(novElement);
    }   
    
  });

  
  socket.on('sporocilo', function (sporocilo) {
    var badWords = $('#badWords').text();
    var stevec = sporocilo.besedilo.length;
     sporocilo.besedilo = zamenjaj(sporocilo.besedilo);
    if(stevec == sporocilo.besedilo.length){
      
      var novElement = $('<div style="font-weight: bold"></div>').text(nePreklinjaj(sporocilo.besedilo,badWords));
      $('#sporocila').append(novElement);//prejemniku napiše na ekran sporočilo
    } 
   else{
     var novElement1 = $('<div style="font-weight: bold"></div>').html(nePreklinjaj(sporocilo.besedilo,badWords));
      $('#sporocila').append(novElement1);//prejemniku napiše na ekran sporočilo
   }

  });
  
  

  socket.on('prikazGostov', function (rezultat) {
    $('#seznam-gostov').empty();
    for(var i = 0; i < rezultat.stevec; i++) {
      var novElement = $('<div style="font-weight: bold"></div>').text(rezultat.gostje[i]);
      $('#seznam-gostov').append(novElement);
    }
  });
  
  

  socket.on('zasebnoSporocilo', function (sporociloo) {
     var html = 0;
     var smejko = zamenjaj(sporociloo.sporocilo);
     if(smejko.length != sporociloo.sporocilo.length) html = 1;
     var badWords = $('#badWords').text();
     var novElement = nePreklinjaj(smejko,badWords);
    if(html == 1) var napisi =   $('<div style="font-weight: bold"></div>').html(sporociloo.posiljatelj+" (zasebno): "+novElement);
    else var napisi = $('<div style="font-weight: bold"></div>').text(sporociloo.posiljatelj+" (zasebno): "+novElement);
     $('#sporocila').append(napisi);
  });
  
   socket.on('poslano', function (sporociloo) {
    var html = 0;
     var smejko = zamenjaj(sporociloo.sporocilo);
     if(smejko.length != sporociloo.sporocilo.length) html = 1;
     var badWords = $('#badWords').text();
     var novElement = nePreklinjaj(smejko,badWords);
    if(html == 1) var napisi =  $('<div style="font-weight: bold"></div>').html("(zasebno za "+sporociloo.prejemnik+"): "+novElement);
    else var napisi =  $('<div style="font-weight: bold"></div>').text("(zasebno za "+sporociloo.prejemnik+"): "+novElement);
    $('#sporocila').append(napisi);
  });
  
  
  socket.on('neObstaja', function (sporociloo) {
    $('#sporocila').append(divElementEnostavniTekst("Sporocila <"+sporociloo.sporocilo+"> uporabniku z vzdevkom <"+sporociloo.prejemnik+"> ni bilo mogoce posredovati."));
  });


  socket.on('napacnoGeslo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text("Pridruzitev v kanal "+sporocilo.kanal+" ni bilo uspešno, ker je geslo napačno!");
    $('#sporocila').append(novElement);
  });
  
  socket.on('niGesla', function (sporocilo) {
      var novElement = $('<div style="font-weight: bold"></div>').text("Izbrani kanal "+sporocilo.kanal+" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev "+sporocilo.kanal+" ali zahtevajte kreiranje kanala z drugim imenom.");
      $('#sporocila').append(novElement);
    });


  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

   $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  setInterval(function() {
    socket.emit('osvezi');
  }, 1000);
  

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});